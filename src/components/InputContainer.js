import styles from './InputContainer.module.css';

export default function InputContainer({
  children,
  icone,
  className = '',
  style,
}) {
  return (
    <div className={`${styles['input-container']} ${className}`} style={style}>
      <img src={icone} alt='Ícone' />
      {children}
    </div>
  );
}
