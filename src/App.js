import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { Home } from './components/Home';
import { Login } from './components/Login';
import { EmpresaDetalhe } from './components/EmpresaDetalhe';

function App() {
  return (
    <Router>
      <Switch>
        <Route path='/login' exact>
          <Login />
        </Route>
        <Route path='/empresas' exact>
          <Home />
        </Route>
        <Route path='/empresas/:id'>
          <EmpresaDetalhe />
        </Route>
        <Route path='*'>
          <Redirect to='/login' />
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
