import { useEffect, useRef, useState } from 'react';

import logoNav from '../imgs/logo-nav.png';
import iconeLupa from '../imgs/ic-search-copy-2.svg';
import iconeClose from '../imgs/ic-close.svg';

import styles from './Home.module.css';
import InputContainer from './InputContainer';
import Empresas from './Empresas';

export function Home() {
  const [focus, setFocus] = useState(false);
  const [filter, setFilter] = useState('');
  const input = useRef(null);

  useEffect(() => {
    if (focus) input.current.focus();
  }, [focus]);

  const search = (
    <InputContainer icone={iconeLupa} style={{ color: 'var(--white-two)' }}>
      <input
        className='Text-Style-5'
        type='text'
        name='filter'
        id='filter'
        placeholder='Pesquisar'
        value={filter}
        ref={input}
        onChange={({ target }) => setFilter(target.value)}
      />
      <img
        src={iconeClose}
        alt='Ícone'
        style={{ cursor: 'pointer' }}
        onClick={() => setFocus(false)}
      />
    </InputContainer>
  );

  const preSearch = (
    <div className={styles.preSearch}>
      <img
        src={logoNav}
        alt='Logomarca Ioasys'
        style={{ gridColumn: 2, justifySelf: 'center' }}
      />
      <img
        src={iconeLupa}
        alt='Ícone'
        style={{ cursor: 'pointer', justifySelf: 'end' }}
        onClick={() => setFocus(true)}
      />
    </div>
  );

  return (
    <div className={styles.home}>
      <div className={styles.header}>{focus ? search : preSearch}</div>
      <div className={styles.body}>
        {focus ? (
          <Empresas filter={filter} />
        ) : (
          <div className='Text-Style-3'>Clique na busca para iniciar.</div>
        )}
      </div>
    </div>
  );
}
