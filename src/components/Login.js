import { useState } from 'react';
import { Redirect } from 'react-router';
import API from '../api';
import InputContainer from './InputContainer';

import logo from '../imgs/logo-home.png';
import iconeEmail from '../imgs/ic-email.svg';
import iconeCadeado from '../imgs/ic-cadeado.svg';

import styles from './Login.module.css';

export function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [logged, setLogged] = useState();

  const error =
    logged === false ? <span className={styles.error}>!</span> : null;

  return logged === true ? (
    <Redirect to='/empresas' />
  ) : (
    <div className={styles.screen}>
      <div className={styles.card}>
        <img className={styles.logo} src={logo} />

        <div className={`Text-Style-10 ${styles['bem-vindo']}`}>
          BEM-VINDO AO EMPRESAS
        </div>
        <div className={`Text-Style ${styles.texto}`}>
          Lorem ipsum dolor sit amet, contetur adipiscing elit. accumsan.
        </div>

        <InputContainer
          className={styles['input-container']}
          icone={iconeEmail}
        >
          <input
            className='Text-Style-2'
            type='text'
            name='email'
            id='email'
            placeholder='E-mail'
            value={email}
            onChange={({ target }) => {
              setLogged(undefined);
              setEmail(target.value);
            }}
          />
          {error}
        </InputContainer>

        <InputContainer
          className={styles['input-container']}
          icone={iconeCadeado}
          style={{ marginTop: '2rem' }}
        >
          <input
            className='Text-Style-2'
            type='password'
            name='senha'
            id='senha'
            placeholder='Senha'
            value={password}
            onChange={({ target }) => {
              setLogged(undefined);
              setPassword(target.value);
            }}
          />
          {error}
        </InputContainer>
        <div className={styles.errorMsg}>
          {logged === false
            ? 'Credenciais informadas são inválidas, tente novamente.'
            : ''}
        </div>
        <button onClick={() => API.signIn(email, password).then(setLogged)}>
          ENTRAR
        </button>
      </div>
    </div>
  );
}
