import { Link } from 'react-router-dom';

import styles from './Empresa.module.css';

export function Empresa({
  id,
  photo,
  enterprise_name,
  enterprise_type_name,
  city,
  country,
}) {
  return (
    <div className={styles.empresa}>
      <Link to={`empresas/${id}`}>
        <img
          src={`https://empresas.ioasys.com.br/${photo}`}
          alt={`Foto: ${enterprise_name}`}
        />
        <div
          className='Text-Style-6'
          style={{ paddingTop: '1.438rem', alignSelf: 'end' }}
        >
          {enterprise_name}
        </div>
        <div className='Text-Style-7'>{enterprise_type_name}</div>
        <div className='Text-Style-8' style={{ paddingBottom: '2.375rem' }}>
          {city}, {country}
        </div>
      </Link>
    </div>
  );
}
