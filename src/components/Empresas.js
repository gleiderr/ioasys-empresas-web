import { useEffect, useState } from 'react';
import API from '../api';
import { Empresa } from './Empresa';

function useEmpresas(filter) {
  const [empresas, setEmpresas] = useState([]);

  useEffect(() => {
    API.empresas(filter).then(({ enterprises }) => setEmpresas(enterprises));
  }, [filter]);

  return empresas;
}

export default function Empresas({ filter }) {
  const empresas = useEmpresas(filter);

  return !empresas.length ? (
    <div className='Text-Style-3' style={{ color: 'var(--greyish)' }}>
      Nenhuma empresa foi encontrada para a busca realizada.
    </div>
  ) : (
    empresas.map((empresa) => (
      <Empresa
        key={empresa.id}
        id={empresa.id}
        photo={empresa.photo}
        enterprise_name={empresa.enterprise_name}
        enterprise_type_name={empresa.enterprise_type.enterprise_type_name}
        country={empresa.country}
        city={empresa.city}
      />
    ))
  );
}
