import { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import API from '../api';

import iconeRetutn from '../imgs/ic-return.svg';

import styles from './EmpresaDetalhe.module.css';

function useEmpresa(id) {
  const [empresa, setEmpresa] = useState();

  useEffect(() => {
    API.empresa(id).then(({ enterprise }) => setEmpresa(enterprise));
  }, [id]);

  return empresa;
}

export function EmpresaDetalhe() {
  const { id } = useParams();
  const empresa = useEmpresa(id);

  return (
    <div className={styles.home}>
      <div className={styles.header}>
        <div className={styles.title}>
          <Link to='/empresas'>
            <img src={iconeRetutn} alt='Ícone' />
          </Link>
          <span className='Text-Style-5'>{empresa?.enterprise_name}</span>
        </div>
      </div>
      <div>
        {!!empresa && (
          <div className={styles.main}>
            <img
              src={`https://empresas.ioasys.com.br/${empresa.photo}`}
              alt={`Foto: ${empresa.enterprise_name}`}
            />
            <div className={styles.texto}>{empresa.description}</div>
          </div>
        )}
      </div>
    </div>
  );
}
