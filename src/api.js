/**
 * Classe responsável por realizar integrações com API da Ioasys.
 * Todos seus métodos e campos são estáticos.
 */
export default class API {
  /**
   * Setter responsável por abstrair atribuição de credenciais do usuário
   * conectado.
   * Valores são armazenado no localStorage do navegador.
   */
  static set credentials({ accessToken, client, uid }) {
    localStorage.setItem('accessToken', accessToken);
    localStorage.setItem('client', client);
    localStorage.setItem('uid', uid);
  }

  /**
   * Getter responsável por abstrair leitura de credenciais do usuário
   * conectado.
   * Valores são armazenado no localStorage do navegador.
   */
  static get credentials() {
    return {
      accessToken: localStorage.getItem('accessToken'),
      client: localStorage.getItem('client'),
      uid: localStorage.getItem('uid'),
    };
  }

  /**
   * Método responsável por requisitar novas credenciais para [email] e
   * [password] informados.
   * @param {string} email E-mail do usuário
   * @param {string} password Senha do usuário
   */
  static async signIn(email, password) {
    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    const raw = JSON.stringify({ email, password });

    const requestOptions = {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    try {
      const response = await fetch(
        'https://empresas.ioasys.com.br/api/v1/users/auth/sign_in',
        requestOptions
      );

      if (response.status == 200) {
        this.credentials = {
          accessToken: response.headers.get('access-token'),
          client: response.headers.get('client'),
          uid: response.headers.get('uid'),
        };

        return true;
      } else {
        return false;
      }
    } catch (error) {
      return console.error('error', error);
    }
  }

  /**
   * Método responsável por requisitar empresas a partir do [filter] informado.
   * @param {string} filter String de busca por empresas.
   */
  static empresas(filter) {
    const { accessToken, client, uid } = this.credentials;

    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('access-token', accessToken);
    myHeaders.append('client', client);
    myHeaders.append('uid', uid);

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow',
    };

    return fetch(
      `https://empresas.ioasys.com.br/api/v1/enterprises?name=${filter}`,
      requestOptions
    )
      .then((response) => response.json())
      .catch((error) => console.log('error', error));
  }

  /**
   * Método responsável por requisitar dadas da empresa identificada por [id].
   * @param {string} id Identificador da empresa
   */
  static empresa(id) {
    const { accessToken, client, uid } = this.credentials;

    var myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    myHeaders.append('access-token', accessToken);
    myHeaders.append('client', client);
    myHeaders.append('uid', uid);

    var requestOptions = {
      method: 'GET',
      headers: myHeaders,
      redirect: 'follow',
    };

    return fetch(
      `https://empresas.ioasys.com.br/api/v1/enterprises/${id}`,
      requestOptions
    )
      .then((response) => response.json())
      .catch((error) => console.log('error', error));
  }
}
